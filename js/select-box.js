/**
 * Created by Александр Орлов on 03.02.2016.
 * Директива <select-box>
 *
 *     Пример:
       <select-box
           source="json/data_source.json"  // Расположение данных в формате json (пример: json/data_source.json)
           none-label="Город вылета"       // Наименование, отображаемое при старте (пока не выбран элемент)
           ng-model="result"               // model для сохранения ID выбранной записи
           sourse-type="cities"            // Тип используемого ресурса ( cities / countries)
           used-shortcut="yes"              // Включить поддержку клавиатуры для выбора из списка (Клавиши:up/down/enter)
        ></select-box>
 ОПИСАНИЕ:
     для корректной работы необходимо в тэге html установить атрибут ng-app в значение "moduleSelectBox", либо
     в 19 строке модуля заменить "moduleSelectBox" на нужное значение np-app.
 //TODO:: Сделать скролл списка при выборе, используя клавиатуру
 */
(function(){
    var app = angular.module('moduleSelectBox', [ ]);
    app.filter('startLetter', function () {
            return function (items, firstLetter) {
                var filtered = [];
                if (firstLetter == 'ВСЕ' || (firstLetter == ''))
                    filtered = items;
                else
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        if (firstLetter == item.name.substring(0, 1))
                            filtered.push(item);
                    }
                return filtered;
            };
        })
        .directive('selectBox', function() {
            return {
                restrict: 'E',
                templateUrl: 'template-select-box2.html', //Шаблон внешнего вида списка
                replace:true,
                transclude: true,
                scope: {
                    ngModel: "="
                },
                controller: function($scope,$http,$attrs,keyboardManager){
                    $scope.selectedText = $attrs.noneLabel;
                    $scope.selItem = -1;
                    $scope.usedShortcut = ($attrs.usedShortcut == 'yes') ? true : false;
                    $scope.letters = ['ВСЕ', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ч', 'Ш', 'Э', 'Ю', 'Я'];
                    $scope.dropdown = false;
                    $scope.query = '';
                    $http.get($attrs.source).success(function(response){
                        var resp = response[$attrs.sourseType];
                        for(var i = 0; i < resp.length; i++)
                            resp[i]['active'] = 0;
                        $scope.dataJSON = resp;
                    });
                    keyboardManager.bind('up', function() {
                        if ($scope.dropdown && $scope.usedShortcut)
                            if ($scope.selItem  > 0){
                                $scope.dataJSON[$scope.selItem]['active']=0;
                                $scope.selItem--;
                                $scope.dataJSON[$scope.selItem]['active']=1;
                            } else {
                                $scope.selItem = $scope.dataJSON.length-1;
                                $scope.dataJSON[0]['active']=0;
                                $scope.dataJSON[$scope.selItem]['active']=1;
                            }
                    });
                    keyboardManager.bind('down', function() {
                        if ($scope.dropdown && $scope.usedShortcut){
                            if ($scope.selItem + 1 < $scope.dataJSON.length){
                                if ($scope.selItem!=-1)
                                    $scope.dataJSON[$scope.selItem]['active']=0;
                                $scope.selItem++;
                                $scope.dataJSON[$scope.selItem]['active']=1;
                            } else {
                                $scope.selItem = 0;
                                $scope.dataJSON[$scope.dataJSON.length-1]['active']=0;
                                $scope.dataJSON[$scope.selItem]['active']=1;
                            }
                        }

                    });
                    keyboardManager.bind('enter', function() {
                        if ($scope.dropdown && $scope.usedShortcut){
                            if ($scope.selItem != -1){
                                $scope.dropdown = false;
                                $scope.ngModel = $scope.dataJSON[$scope.selItem].id;
                                $scope.selectedText = $scope.dataJSON[$scope.selItem].name;
                            }
                        }
                    });
                    $scope.setDropdown = function(){
                        $scope.dropdown = !$scope.dropdown;
                        return true;
                    };
                    $scope.selectItem = function(item){
                        $scope.dropdown = false;
                        $scope.ngModel = item.id;
                        $scope.selectedText = item.name;
                        return true;
                    };
                    $scope.setFilter = function ($startLetter){
                        $scope.query = $startLetter == 'ВСЕ' ? '' :$startLetter;
                        return true;
                    };

                }
            }
        })
    .factory('keyboardManager', ['$window', '$timeout', function ($window, $timeout) {
        var keyboardManagerService = {};

        var defaultOpt = {
            'type':             'keydown',
            'propagate':        false,
            'inputDisabled':    false,
            'target':           $window.document,
            'keyCode':          false
        };
        // Store all keyboard combination shortcuts
        keyboardManagerService.keyboardEvent = {}
        // Add a new keyboard combination shortcut
        keyboardManagerService.bind = function (label, callback, opt) {
            var fct, elt, code, k;
            // Initialize opt object
            opt   = angular.extend({}, defaultOpt, opt);
            label = label.toLowerCase();
            elt   = opt.target;
            if(typeof opt.target == 'string') elt = document.getElementById(opt.target);

            fct = function (e) {
                e = e || $window.event;

                // Disable event handler when focus input and textarea
                if (opt['inputDisabled']) {
                    var elt;
                    if (e.target) elt = e.target;
                    else if (e.srcElement) elt = e.srcElement;
                    if (elt.nodeType == 3) elt = elt.parentNode;
                    if (elt.tagName == 'INPUT' || elt.tagName == 'TEXTAREA') return;
                }

                // Find out which key is pressed
                if (e.keyCode) code = e.keyCode;
                else if (e.which) code = e.which;
                var character = String.fromCharCode(code).toLowerCase();

                if (code == 188) character = ","; // If the user presses , when the type is onkeydown
                if (code == 190) character = "."; // If the user presses , when the type is onkeydown

                var keys = label.split("+");
                // Key Pressed - counts the number of valid keypresses - if it is same as the number of keys, the shortcut function is invoked
                var kp = 0;
                // Work around for stupid Shift key bug created by using lowercase - as a result the shift+num combination was broken
                var shift_nums = {
                    "`":"~",
                    "1":"!",
                    "2":"@",
                    "3":"#",
                    "4":"$",
                    "5":"%",
                    "6":"^",
                    "7":"&",
                    "8":"*",
                    "9":"(",
                    "0":")",
                    "-":"_",
                    "=":"+",
                    ";":":",
                    "'":"\"",
                    ",":"<",
                    ".":">",
                    "/":"?",
                    "\\":"|"
                };
                // Special Keys - and their codes
                var special_keys = {
                    'esc':27,
                    'escape':27,
                    'tab':9,
                    'space':32,
                    'return':13,
                    'enter':13,
                    'backspace':8,

                    'scrolllock':145,
                    'scroll_lock':145,
                    'scroll':145,
                    'capslock':20,
                    'caps_lock':20,
                    'caps':20,
                    'numlock':144,
                    'num_lock':144,
                    'num':144,

                    'pause':19,
                    'break':19,

                    'insert':45,
                    'home':36,
                    'delete':46,
                    'end':35,

                    'pageup':33,
                    'page_up':33,
                    'pu':33,

                    'pagedown':34,
                    'page_down':34,
                    'pd':34,

                    'left':37,
                    'up':38,
                    'right':39,
                    'down':40,

                    'f1':112,
                    'f2':113,
                    'f3':114,
                    'f4':115,
                    'f5':116,
                    'f6':117,
                    'f7':118,
                    'f8':119,
                    'f9':120,
                    'f10':121,
                    'f11':122,
                    'f12':123
                };
                // Some modifiers key
                var modifiers = {
                    shift: {
                        wanted:		false,
                        pressed:	e.shiftKey ? true : false
                    },
                    ctrl : {
                        wanted:		false,
                        pressed:	e.ctrlKey ? true : false
                    },
                    alt  : {
                        wanted:		false,
                        pressed:	e.altKey ? true : false
                    },
                    meta : { //Meta is Mac specific
                        wanted:		false,
                        pressed:	e.metaKey ? true : false
                    }
                };
                // Foreach keys in label (split on +)
                for(var i=0, l=keys.length; k=keys[i],i<l; i++) {
                    switch (k) {
                        case 'ctrl':
                        case 'control':
                            kp++;
                            modifiers.ctrl.wanted = true;
                            break;
                        case 'shift':
                        case 'alt':
                        case 'meta':
                            kp++;
                            modifiers[k].wanted = true;
                            break;
                    }

                    if (k.length > 1) { // If it is a special key
                        if(special_keys[k] == code) kp++;
                    } else if (opt['keyCode']) { // If a specific key is set into the config
                        if (opt['keyCode'] == code) kp++;
                    } else { // The special keys did not match
                        if(character == k) kp++;
                        else {
                            if(shift_nums[character] && e.shiftKey) { // Stupid Shift key bug created by using lowercase
                                character = shift_nums[character];
                                if(character == k) kp++;
                            }
                        }
                    }
                }

                if(kp == keys.length &&
                    modifiers.ctrl.pressed == modifiers.ctrl.wanted &&
                    modifiers.shift.pressed == modifiers.shift.wanted &&
                    modifiers.alt.pressed == modifiers.alt.wanted &&
                    modifiers.meta.pressed == modifiers.meta.wanted) {
                    $timeout(function() {
                        callback(e);
                    }, 1);

                    if(!opt['propagate']) { // Stop the event
                        // e.cancelBubble is supported by IE - this will kill the bubbling process.
                        e.cancelBubble = true;
                        e.returnValue = false;

                        // e.stopPropagation works in Firefox.
                        if (e.stopPropagation) {
                            e.stopPropagation();
                            e.preventDefault();
                        }
                        return false;
                    }
                }

            };
            // Store shortcut
            keyboardManagerService.keyboardEvent[label] = {
                'callback': fct,
                'target':   elt,
                'event':    opt['type']
            };
            //Attach the function with the event
            if(elt.addEventListener) elt.addEventListener(opt['type'], fct, false);
            else if(elt.attachEvent) elt.attachEvent('on' + opt['type'], fct);
            else elt['on' + opt['type']] = fct;
        };
        // Remove the shortcut - just specify the shortcut and I will remove the binding
        keyboardManagerService.unbind = function (label) {
            label = label.toLowerCase();
            var binding = keyboardManagerService.keyboardEvent[label];
            delete(keyboardManagerService.keyboardEvent[label])
            if(!binding) return;
            var type		= binding['event'],
                elt			= binding['target'],
                callback	= binding['callback'];
            if(elt.detachEvent) elt.detachEvent('on' + type, callback);
            else if(elt.removeEventListener) elt.removeEventListener(type, callback, false);
            else elt['on'+type] = false;
        };
        //
        return keyboardManagerService;
    }]);
})();